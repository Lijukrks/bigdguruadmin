var jwt = require('jsonwebtoken');
module.exports = {
    auth: function securityHandler(req, authOrSecDef, scopesOrApiKey, cb) {
        console.log("verifying auth")
        var token =req.swagger.params.token.value;          
        console.log("token is" ,token);  
        jwt.verify(token, "key", function(err, decoded) {      
            if (err) {
                cb(new Error("Error validating token"))
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;    
                cb();
            }
        });    
    }    
}