var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var config = require('../configs/serverConfig.json');
var User = require('../models/user');

module.exports = function(passport){
	passport.use(new FacebookStrategy({
    	clientID: config.auth.facebook.clientID,
    	clientSecret: config.auth.facebook.clientSecret,
    	callbackURL: config.auth.facebook.callbackURL,
        profileFields: ['id', 'displayName','picture.type(large)']
  	},
  	function(accessToken, refreshToken, profile, cb) {
    	console.log("fb data",JSON.stringify(profile))
    	User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
    		console.log("Checking mongo")
            if (err){
                return done(err);
                console.log("Error connecting to db")
            }
            if (user) {
            	console.log("Found user");
                return cb(null, user);
            } else {
            	console.log("No user in db");
                var newUser            = new User();
                newUser.facebook.id    = profile.id;                
                newUser.facebook.name  = profile.displayName;
                if (profile.photos.length > 0) {
                    picUrl = profile.photos[0]["value"];
                    newUser.facebook.profilePicUrl = picUrl;
                }
                else{
                    newUser.facebook.profilePicUrl ="";
                }
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return cb(null, newUser);
                });
            }

        });
		
    }));

    passport.use(new TwitterStrategy({
        consumerKey: config.auth.twitter.consumerKey,
        consumerSecret: config.auth.twitter.consumerSecret,
        callbackURL: config.auth.twitter.callbackURL,
        profileFields: ['id', 'displayName','picture.type(large)','name']
    },
    function(accessToken, refreshToken, profile, cb) {
        console.log("twitter data",JSON.stringify(profile))
        User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
            console.log("Checking mongo")
            if (err){
                return done(err);
                console.log("Error connecting to db")
            }
            if (user) {
                console.log("Found user");
                return cb(null, user);
            } else {
                console.log("No user in db");
                var newUser           = new User();
                newUser.twitter.id    = profile.id;                
                newUser.twitter.name  = profile.name||profile.username;
                if (profile.photos.length > 0) {
                    picUrl = profile.photos[0]["value"];
                    newUser.twitter.profilePicUrl = picUrl.replace("_normal","_bigger");

                }
                newUser.save(function(err) {
                    if (err)
                        throw err;

                    return cb(null, newUser);
                });
            }

        });
        
    }));

    passport.use(new LinkedInStrategy({
        clientID: config.auth.linkedin.clientID,
        clientSecret: config.auth.linkedin.clientSecret,
        callbackURL: config.auth.linkedin.callbackURL,
        profileFields: ['id','first-name','formatted-name','last-name','picture-urls::(original)'],
        state: "true"
    },
    function(token, tokenSecret, profile, done) {
        console.log("Done linkeding auth ",profile);
        User.findOne({ 'linkedin.id' : profile.id }, function(err, user) {
            console.log("Checking mongo")
            if (err){
                return done(err);
                console.log("Error connecting to db")
            }
            if (user) {
                console.log("Found user");
                return done(null, user);
            } else {
                console.log("No user in db");
                var newUser           = new User();
                newUser.linkedin.id    = profile.id;                
                newUser.linkedin.name  = profile.displayName;
                if (profile.photos.length > 0) {
                    picUrl = profile.photos[0]["value"];
                    newUser.linkedin.profilePicUrl = picUrl;
                }
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }
        });
    }));   
}
