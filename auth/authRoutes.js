var jwt    = require('jsonwebtoken');
var jsdom = require('jsdom').jsdom;
var document = jsdom('<html></html>', {});
var window = document.defaultView;
var $ = require('jquery')(window);
var request = require('request');
//var host = "52.33.234.9";
var host = "localhost";

module.exports = function(app,passport){
  var tokenValue ="";

	app.get('/auth/facebook',
  passport.authenticate('facebook',{session: false}));
  app.get('/auth/facebook/callback', function(req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
      if (err) {
          console.log("err",err)
      } else
      {
        token = jwt.sign(user, "secret", {
        expiresInMinutes: 1440 // expires in 24 hours
      });
      var tokenValue = token;
      var name ="";
      var id ="";
      var profilePicUrl ="";
      var urlValue = "http://"+host+":4000/";
      var options = {
        url:urlValue+ 'profile',
        headers: {
          "token": tokenValue
          }
        };
      function callback(error, response, body) {
        if (!error) {
          var info = JSON.parse(body);
          console.log("BODY.....",info);
          console.log("the end",info["_doc"]["facebook"]["id"],info["_doc"]["facebook"]["name"]);
          name = info["_doc"]["facebook"]["name"];
          id =info["_doc"]["facebook"]["id"];
          profilePicUrl =info["_doc"]["facebook"]["profilePicUrl"];
          var data = {
            token : token,
            name : name,
            id : id,
            profilePicUrl : profilePicUrl
          };
          res.redirect("/#/acceptAuth?userData=" + encodeURIComponent(JSON.stringify(data)));
        }
        else{
        console.log(error);
        }
      }
      request(options, callback);
      //res.send(token);
      }
    })(req, res, next);
  });

  app.get('/auth/twitter',
  passport.authenticate('twitter'));
  app.get('/auth/twitter/callback', function(req, res, next) {
    passport.authenticate('twitter', function(err, user, info) {
      if (err) {
        console.log("Twitter ERROR",err);
      } else{
        var token = jwt.sign(user, "secret", {
          expiresInMinutes: 1440 // expires in 24 hours
        });
        console.log("info",info);
        var tokenValue = token;
        var name ="";
        var id ="";
        var profilePicUrl ="";
        var options = {
          url:"http://"+host+":4000/"+ 'profile',
          headers: {
            "token": tokenValue
            }
          };
        function callback(error, response, body) {
          if (!error) {
            var info = JSON.parse(body);
            console.log("BODY.....",info);
            //name = info["_doc"]["twitter"]["name"];
            name = info["_doc"]["twitter"]["name"];;
            id =info["_doc"]["twitter"]["id"];
            profilePicUrl =info["_doc"]["twitter"]["profilePicUrl"];
            var data = {
              token : token,
              name : name,
              id : id,
              profilePicUrl : profilePicUrl
            };
            console.log ("main data",data);
            res.redirect("/#/acceptAuth?userData=" + encodeURIComponent(JSON.stringify(data)));
          }
          else{
            console.log(error);
          }
        }
        request(options, callback);
      }

    })(req, res, next);
  });

  app.get('/auth/linkedin',
  passport.authenticate('linkedin',{session: false}));
  app.get('/auth/linkedin/callback', function(req, res, next) {
    passport.authenticate('linkedin', function(err, user, info) {
      if (err) {
        console.log("err",err)
      }else{
          var token = jwt.sign(user, "secret", {
          expiresInMinutes: 1440 // expires in 24 hours
        });
        console.log("info",info);            
        var tokenValue = token;
        var name ="";
        var id ="";
        var profilePicUrl ="";
        var options = {
          url:"http://"+host+":4000/"+ 'profile',
          headers: {
            "token": tokenValue
          }
        };
        function callback(error, response, body) {
          if (!error) {
            var info = JSON.parse(body);
            console.log("BODY.....",info);
            name = info["_doc"]["linkedin"]["name"];
            id =info["_doc"]["linkedin"]["id"];
            profilePicUrl =info["_doc"]["linkedin"]["profilePicUrl"];
            var data = {
              token : token,
              name : name,
              id : id,
              profilePicUrl : profilePicUrl
            };
            console.log ("main data",data);
            res.redirect("/#/acceptAuth?userData=" + encodeURIComponent(JSON.stringify(data)));
          }
          else{
            console.log(error);
          }
        }
        request(options, callback);
        //res.send({"token":token,"user":user});
      }
    })(req, res, next);
  });

}
