var searchResult = require('../helpers/QueueSearchService')
module.exports = {
    getQueueSearchResults:getQueueSearchResults
};
function getQueueSearchResults(req,res){
	var searchBody = req.swagger.params.pendingNewsBody.value;
	var searchKeyword = searchBody.searchKeyword;
	var pageNumber=searchBody.pageNumber
	
	 searchResult.searchResultEs(searchKeyword,pageNumber,function (error, response){
	    if (error) {
	        console.log("Pending news controller error......",error);
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
