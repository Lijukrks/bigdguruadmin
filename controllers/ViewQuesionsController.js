var searchResult = require('../helpers/ViewQuesionsService')
module.exports = {
    getQuestions:getQuestions
};
function getQuestions(req,res){
	var searchBody = req.swagger.params.questionsBody.value;
	var searchKeyword = searchBody.searchKeyword;
	var pageNumber = searchBody.pageNumber;
	 searchResult.searchResultEs(searchKeyword,pageNumber,function (error, response){
	    if (error) {
	        console.log(" Search result error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
