var GetNewsService = require('../helpers/GetNewsService')
module.exports = {
    GetNews:GetNews
};
function GetNews(req,res){
	var GetNewsBody = req.swagger.params.GetNewsBody.value;
	var url = GetNewsBody.url;
	
	 GetNewsService.getNewsEs(url,function (error, response){
	    if (error) {
	        console.log(" get news error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response ....................................",response)
	        res.json(response);
	    }
    });
    
}
