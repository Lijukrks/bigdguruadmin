var searchResult = require('../helpers/newsService')
module.exports = {
    publishedNewsResults:publishedNewsResults
};
function publishedNewsResults(req,res){
	var searchBody = req.swagger.params.publishedNewsBody.value;
	var searchKeyword = searchBody.searchKeyword;
	var pageNumber=searchBody.pageNumber
	 searchResult.searchResultEs(searchKeyword,pageNumber,function (error, response){
	    if (error) {
	        console.log(" Search result error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
