var publish = require('../helpers/RemoveNewsFromSearchQueueService')
module.exports = {
    removeNewsFromSearchQueue:removeNewsFromSearchQueue
};
function removeNewsFromSearchQueue(req,res){
	var searchBody = req.swagger.params.removeNewsFromSearchQueueBody.value;
	var id = searchBody.id;
	console.log("id",id)
	
	 publish.removeNewsFromQueueEs(id,function (error, response){
	    if (error) {
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
