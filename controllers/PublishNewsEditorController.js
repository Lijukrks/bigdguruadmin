var searchResult = require('../helpers/PublishNewsEditorService')
module.exports = {
    getPublishNewsEditorResults:getPublishNewsEditorResults
};
function getPublishNewsEditorResults(req,res){
	var searchBody = req.swagger.params.PublishNewsEditorBody.value;
	var searchKeyword = searchBody.searchKeyword;
	console.log("searchKeywordNews.......",searchKeyword)
	
	 searchResult.searchResultEs(searchKeyword,function (error, response){
	    if (error) {
	        console.log(" Search result error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
