var searchResult = require('../helpers/NewsEditorService')
module.exports = {
    getNewsEditorResults:getNewsEditorResults
};
function getNewsEditorResults(req,res){
	var searchBody = req.swagger.params.NewsEditorBody.value;
	var searchKeyword = searchBody.searchKeyword;
	console.log("searchKeywordNews.......",searchKeyword)
	
	 searchResult.searchResultEs(searchKeyword,function (error, response){
	    if (error) {
	        console.log(" Search result error occured");
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
