var AddNewsService = require('../helpers/AddNewsService')
module.exports = {
    AddNews:AddNews
};
function AddNews(req,res){
	var AddNewsBody = req.swagger.params.AddNewsBody.value;
	var newsBody = AddNewsBody.newsBody;
	var questionTitle = newsBody.title
	var newsId = questionTitle.replace(/[^a-zA-Z ]/g, "");
   	newsId = newsId.replace(/ /g,'-')
	AddNewsService.addNewsEs(newsBody,newsId,function (error, response){
	    if (error) {
	        console.log(" get news error occured",error);
	        res.json({});
	    }
	    else
	    {
	    	console.log("response ....................................",response)
	        res.json(response);
	    }
    });
    
}
