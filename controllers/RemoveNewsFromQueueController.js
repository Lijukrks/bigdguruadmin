var publish = require('../helpers/RemoveNewsFromQueueService')
module.exports = {
    removeNewsFromQueue:removeNewsFromQueue
};
function removeNewsFromQueue(req,res){
	var searchBody = req.swagger.params.removeNewsFromQueueBody.value;
	var id = searchBody.id;
	console.log("id",id)
	
	 publish.removeNewsFromQueueEs(id,function (error, response){
	    if (error) {
	        res.json({});
	    }
	    else
	    {
	    	console.log("response",response)
	        res.json(response);
	    }
    });
    
}
