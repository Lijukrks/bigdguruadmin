var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')
var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	publishNewsEs: function(newsBody,id,callBack) {
		console.log("newsbody",newsBody)
		client.create({
		  index: esConfig.newsIndex,
		  type: esConfig.type,
		  id:id,
		  body: newsBody
		}, function (error,response) {
		     if (error) {
		        callBack(error, null);
		      } else {
		        callBack(null, response);
		      }
		});
	}
}