var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')
var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	addNewsEs: function(newsBody,newsId,callBack) {
		console.log("newsBodyservive",newsBody)
		client.create({
		  index: esConfig.queueIndex,
		  type: esConfig.type,
		  body: newsBody,
		  id:newsId
		}, function (error,response) {
		     if (error) {
		        callBack(error, null);
		      } else {
		        callBack(null, response);
		      }
		});
	}
}