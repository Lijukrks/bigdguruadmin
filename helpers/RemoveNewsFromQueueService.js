var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')

var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	removeNewsFromQueueEs: function(newsId,callBack) {
		client.delete({
		  index: esConfig.queueIndex,
		  type: esConfig.type,
		  id: newsId
		}, function (error,response) {
		     if (error) {
		        callBack(error, null);
		      } else {
		      	console.log("removed")
		        callBack(null, response);
		      }
		});
	}
}