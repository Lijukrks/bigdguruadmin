var User = require('../models/user');

module.exports = {
	SignUp: function(SignUpBody,callBack) {
		var FirstName = SignUpBody.FirstName;
       var LastName = SignUpBody.LastName;
       var Mail = SignUpBody.Mail;
       var password = SignUpBody.password
       var newUser = new User({'login.FirstName':FirstName,'login.LastName':LastName,'login.Mail':Mail,'login.password':password})

       //save entry to MongoDB
       newUser.save(function (err,rsp) {
         if (err) {
          console.log("error")
           callBack(err, null);
         }
         else {
           callBack(rsp, null);
         }
     });
	}
}