var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')

var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	updateNewsEs: function(newsBody,docId,callBack) {
		console.log("newsBody",newsBody)
		client.update({
		  index: esConfig.newsIndex,
		  type: esConfig.type,
		  id: docId,
		  body: {"doc":newsBody}
		}, function (error,response) {
		     if (error) {
		        callBack(error, null);
		      } else {
		        callBack(null, response);
		      }
		});
	}
}