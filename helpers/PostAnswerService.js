var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
	host: 'http://52.33.234.9:9200/'
});

module.exports = {
	postAnswerEs: function(answerBody,callBack) {
		client.create({
		  index: 'answer01',
		  type: 'answer',
		  body: answerBody
		}, function (error,response) {
		     if (error) {
		        callBack(error, null);
		      } else {
		        callBack(null, response);
		      }
		});
	}
}