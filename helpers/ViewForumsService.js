var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')

var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	searchResultEs: function (pageNumber,callBack) {
		var queryBody ;
			console.log("reached")
			queryBody = {
				  "from": pageNumber,
				  "query": {
				    "match_all": {}
				  },
				  "sort": {
				    "dateTime": {
				      "order": "desc"
				    }
				  }
				}
		client.search({
		index: esConfig.forumIndex,
		type: esConfig.forumtype,
		body: queryBody
		}, function (error, response) {
			if (error) {
				callBack(error, null);
			} else {
				callBack(null, response);
			}
		});
	}
}