var elasticsearch = require('elasticsearch');
var esConfig = require('../configs/esConfigs.js')
var client = new elasticsearch.Client({
	host: esConfig.host
});

module.exports = {
	searchResultEs: function (searchKeyword,pageNumber, callBack) {
		console.log("keywotrd",searchKeyword)
		var queryBody ;
		
		if(!searchKeyword){
			console.log("reached")
			queryBody={
				"from":pageNumber,
				"size":20,
			  "query": {
			    "match_all": {}
			  },"sort": {
    "date": {
      "order": "desc"
    }
  }
			}
		}
		client.search({
		index: esConfig.newsIndex,
		body: queryBody
		}, function (error, response) {
			if (error) {
				callBack(error, null);
			} else {
				callBack(null, response);
			}
		});
	}
}