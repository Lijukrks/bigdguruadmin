var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
	host: 'http://localhost:9200/'
});
module.exports = {
	searchKeyword: function (keyword, callBack) {
		client.search({
			index: "algotree",
			indexType: "news",
			body: {
				"query": {
					"match_all": {}
				}
			}
		}, function (error, response) {
			if (error) {
				callBack(error, null);
			} else {
				callBack(null, response);
			}
		});
	},

	productKeyword: function (productSearchKeyword, callBack) {
		client.search({
			index: "algotree",
			indexType: "news",
			body: {
				"query": {
					"match_all": {}
				}
			}
		}, function (error, response) {
			if (error) {
				callBack(error, null);
			} else {
				callBack(null, response);
			}
		});
	}


}