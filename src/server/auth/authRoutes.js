var jwt    = require('jsonwebtoken');

module.exports = function(app,passport){
	app.get('/auth/facebook',
    passport.authenticate('facebook',{session: false}));

    app.get('/auth/facebook/callback', function(req, res, next) {
      passport.authenticate('facebook', function(err, user, info) {
        if (err) {
            console.log("err",err)
        } else{
            var token = jwt.sign(user, "secret", {
             	expiresInMinutes: 1440 // expires in 24 hours
            });    
            res.send(token);    
        }
    
      })(req, res, next);
    });
}