var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('../configs/serverConfig.json');

module.exports = function(passport){

	passport.use(new FacebookStrategy({
    	clientID: config.auth.facebook.clientID,
    	clientSecret: config.auth.facebook.clientSecret,
    	callbackURL: config.auth.facebook.callbackURL
  	},
  	function(accessToken, refreshToken, profile, cb) {
    	console.log("fb data",JSON.stringify(profile))
    	return cb(null,profile);
  	}));
}