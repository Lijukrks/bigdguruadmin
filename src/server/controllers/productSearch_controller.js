var es = require('../helpers/ElasticSearchService');
module.exports = {
	productKeywordGET: productKeywordGET
};

function productKeywordGET(req, res) {
	var productSearchKeyword = req.swagger.params.productKeyword.value;
	es.productKeyword(productSearchKeyword, function (error, response) {
		if (error) {
			console.log(error);
		} else {
			console.log('response', response);
			res.json(response);
		}

	});


}