var app = require('express')();
var swaggerTools = require('swagger-tools');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var jwt = require('jsonwebtoken');
var path = require('path');
require('./auth/passport')(passport);
require('./auth/authRoutes')(app, passport)
var webpackconfig =require('../../webpack.config')

var serverPort = 4000;

// swaggerRouter configuration
var options = {
	controllers: './controllers',
	useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
}

var config = {
	appRoot: __dirname // required config
}

config.swaggerSecurityHandlers = require('./auth/securityHandler');

var swaggerDoc = require('./api/swagger.json');
var webpack = require('webpack');
var webpackMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
//app.use(express.static('../drone/public'));
const compiler =webpack(webpackconfig);
const middleware =webpackMiddleware(compiler,{
	publicPath: webpackconfig.output.publicPath,
	constentBase:'dist',
	stats:{
		color:true,
		hash:false,
		timings:true,
		chunks:false,
		chunkModules:false,
		modules:false
	}
	
});
app.use(middleware);
app.use(webpackHotMiddleware(compiler));
app.get('*',function response(req,res){
	res.write(middleware.fileSystem.readFileSync(path.join(_dirname,'dist/index.html')));
	res.end();
});

app.use(express.static('src/drone/'));
app.use(express.static('src/drone/public/'));
// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
	// Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
	app.use(middleware.swaggerMetadata());
	app.use(passport.initialize())
	app.use(middleware.swaggerSecurity(config.swaggerSecurityHandlers));

	app.use(function (err, req, res, next) {
		console.error("Security Error Middleware: " + JSON.stringify(err));
		if (err.statusCode == 403) {
			var error = {};
			error.code = 1006;
			error.status = 403;
			error.message = 'Authorization Failed.';
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify(error));
		}
	});

	// Validate Swagger requests
	app.use(middleware.swaggerValidator());

	// Route validated requests to appropriate controller

	app.use(middleware.swaggerRouter(options));
	app.use(require('body-parser').urlencoded({
		extended: true
	}));

	// Serve the Swagger documents and Swagger UI
	app.use(middleware.swaggerUi());

	// Start the server
	app.listen(serverPort, function () {
		console.log('Your server is listening * on port %d (http://localhost:%d)', serverPort, serverPort);
	});
});
