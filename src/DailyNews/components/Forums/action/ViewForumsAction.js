import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Forums/constants/ViewForumsConstant';
import config from 'utils/config'

var ViewForumsAction = function(){

}


ViewForumsAction.prototype = {
	 ViewAction:function(searchObject){
		    $.ajax({
					url: config.server + 'ViewForums',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(searchObject),
					success: function(resp){
						console.log("ViewForumsAction...",resp)
							AppDispatcher.dispatch({
					        actionType: Constants.VIEW_FORUMS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new ViewForumsAction();
