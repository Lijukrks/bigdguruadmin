import React from 'react';
import ViewForumsAction from 'components/Forums/action/ViewForumsAction'
import ViewForumsStore from 'components/Forums/store/ViewForumsStore'
import ForumsTable from 'components/ForumsTable'

require('rc-pagination/assets/index.css');
var Pagination = require('rc-pagination');
var moment = require('moment');
var Forums = React.createClass({
		getInitialState:function(){
			return{
				forumResponse:'',
				isInitial:true,
				defaultCurrent:1
			}
		},
		componentWillMount:function(){
			ViewForumsStore.bind(this.onResponse)
			ViewForumsAction.ViewAction({"pageNumber":0})
		},
		onResponse:function(){
			this.setState({
				forumResponse:ViewForumsStore.getSearchResponse(),
				isInitial:false
			})
		},
		onPageChange: function(pageNum){
			this.setState({
              isInitial:true,
              defaultCurrent:pageNum
          })
		    pageNum = (pageNum - 1) * 10;
		    ViewForumsAction.ViewAction({"pageNumber":pageNum})
		  },
		render: function(){
			if(this.state.isInitial){
				return(<div className="loader container-fluid"></div>)
			}else{
				var data = this.state.forumResponse.data;
				console.log("Queue",data)
				var columns = [ "Title" , "Date","PostedBy","Action"]
		          var tableData = []
		          var singleRow ={}
		          for(var i = 0, n = data.length; i < n ; i++){
		            singleRow={}
		            singleRow ={"Title":data[i].questionTitle,
		                        "Date":moment(data[i].dateTime).format("YYYY-MM-DD"),
		                        "Discription":data[i].questionDescription,
		                        "PostedBy":data[i].user.userName
		          }
		                     tableData.push(singleRow)
		                   }
		                   console.log("data........",data)
		          return ( 	
							<div>
								<ForumsTable
								columns ={columns} 
								data = {tableData} 
							/>
							<Pagination 
		                      defaultCurrent={this.state.defaultCurrent} 
		                      total={this.state.forumResponse.totalHits}
		                      onChange={this.onPageChange}
		                  	/>
							</div>
                );
			}
				
		}
});

module.exports = Forums;
