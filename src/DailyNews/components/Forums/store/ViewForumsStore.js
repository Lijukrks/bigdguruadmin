var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/ViewForumsConstant');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';
var ForumsPage = {
      data:[]
    }
function parseResponse(resp){
    ForumsPage = {
      data:[]
    }
    var jsonData = resp.hits.hits
    for(var i=0;i<jsonData.length;i++){
        var content={}
        if(jsonData[i]._source && jsonData[i]._source.dateTime)
        {
            content.dateTime = jsonData[i]._source.dateTime
        }
        if(jsonData[i]._source && jsonData[i]._source.comment)
        {
            content.comment = jsonData[i]._source.comment
        }
        if(jsonData[i]._source && jsonData[i]._source.questionDescription)
        {
            content.questionDescription = jsonData[i]._source.questionDescription
        }
        if(jsonData[i]._source && jsonData[i]._source.questionTitle)
        {
            content.questionTitle = jsonData[i]._source.questionTitle
        }
        if(jsonData[i]._source && jsonData[i]._source.tags)
        {
            content.tags = jsonData[i]._source.tags
        }
        if(jsonData[i]._source && jsonData[i]._source.view)
        {
            content.view = jsonData[i]._source.view
        }
        if(jsonData[i]._source && jsonData[i]._source.user)
        {
            content.user = jsonData[i]._source.user
        }
        if(jsonData[i]._id)
        {
            content.id = jsonData[i]._id
        }
        
         ForumsPage.data.push(content)
    }
    ForumsPage.totalHits=resp.hits.total
}

var ViewForumsStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getSearchResponse:function(){
        return ForumsPage
    }
});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.VIEW_FORUMS_RESPONSE_RECIEVED:
            var resp = action.data;
            console.log("ViewForumsStore......",resp)
            parseResponse(resp)
            ViewForumsStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = ViewForumsStore;
