import React from 'react';
var ForumsTable = React.createClass({
    render: function(){
      console.log("columns",this.props.columns,"data",this.props.data)
    	var singleRow = []
    	var TableHead = []
    	var TableContent=this.props.data
    	var columns = this.props.columns
    	for(var i=0;i<columns.length;i++){
    		var item=<th key ={i}>
	                  			{columns[i]}
	                 </th>
	        TableHead.push(item)
    	 }
      	for(var i=0;i<TableContent.length;i++){
      		var item=<tr key ={i}>
                    	<td>
                    		{TableContent[i].Title}
                    	</td>
                    	<td>
                    		{TableContent[i].Date}
                    	</td>
                      <td>
                        {TableContent[i].PostedBy}
                      </td>
                    </tr>
      		singleRow.push(item)

      	}
        return ( 
        		<table>
        				<thead>
	                  	<tr>
	                  		{TableHead}
	                  	</tr>
	                  </thead>
	                  <tbody>
	                  		{singleRow}
	                  </tbody>
        		</table>
                );
    }
});

module.exports = ForumsTable;
