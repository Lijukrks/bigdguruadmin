import React from 'react';
import NewsEditor from 'components/NewsEditor'
var AddNewsWrapper= React.createClass({
  publishNewsHandler:function(newsBody){
  },
    
    render: function(){
            var buttons = [ 
                        { "onClickCallback" : this.publishNewsHandler , "label" : "Publish", "className" : "control-btn"}
                    ];
      
      var dataEditor =JSON.parse(this.props.data)
        return ( 
                <div>
                    <div className="features-block">
                     
                    </div>
                    <div className="editor-block">
                      <NewsEditor
                        buttons={buttons}
                        data={dataEditor}
                      />
                    </div>
                      <div className="footer-action">
                      {this.state.isPublishNews && <div>Succesfully Published News</div>}
                    </div>
                </div>
                );
      }
});

module.exports = AddNewsWrapper;
