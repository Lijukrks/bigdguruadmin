import React from 'react';
import NewsEditor from 'components/NewsEditor'
import NewsEditorAction from 'components/PendingNewsEditorWrapper/action/NewsEditorAction'
import NewsEditorStore from 'components/PendingNewsEditorWrapper/store/NewsEditorStore'
import PublishNewsAction from 'components/PendingNewsEditorWrapper/action/PublishNewsAction'
import PublishNewsStore from 'components/PendingNewsEditorWrapper/store/PublishNewsStore'
import RemoveNewsFromQueueAction from 'components/PendingNewsEditorWrapper/action/RemoveNewsFromQueueAction'
import RemoveNewsFromQueueStore from 'components/PendingNewsEditorWrapper/store/RemoveNewsFromQueueStore'
import { hashHistory } from 'react-router'
var moment = require("moment");
var cookie = require("public/js/cookie.js")
var PendingNewsEditorWrapper= React.createClass({

    getInitialState:function(){
         return{
            isInitial : true,
            NewsEditorResult:[],
            isPublishNews:false,
            isCreated:false,
            isRemoved:false
         }
    },
    componentWillMount:function(){
        NewsEditorStore.bind(this.onNewsResponse)
        PublishNewsStore.bind(this.publishNews)
        RemoveNewsFromQueueStore.bind(this.removeNews)
        NewsEditorAction.EditorAction({'searchKeyword':this.props.params.id})

    },
    componentWillUnMount:function(){
        NewsEditorStore.unbind(this.onNewsResponse)
        RemoveNewsFromQueueStore.unbind(this.removeNews)
        PublishNewsStore.unbind(this.publishNews)
    },
    onNewsResponse : function(){
      this.setState({
                isInitial : false,
                NewsEditorResult:NewsEditorStore.getNewsEditorResponse()
            })
    },
    publishNewsHandler:function(newsBody){
      console.log("newsBody publish",newsBody)
      if(!this.state.isPublishNews){
        var publishNewsBody ={
          "date":newsBody.date,
          "imageUrl":newsBody.imageUrl,
          "link":newsBody.link,
          "content":newsBody.content,
          "htmlstrip":newsBody.htmlstrip,
          "title":newsBody.title,
          "tags":newsBody.tags,
          "author":newsBody.author,
          "view":0,
          "comment":0,
          "pablishedAuther":cookie.readCookie("Username"),
          "postedAuther": this.state.NewsEditorResult.data[0].postedAuther
        }
        if(confirm("Press Ok to Publish!") == true){
          PublishNewsAction.addAction({'newsBody':publishNewsBody,"id":this.state.NewsEditorResult.data[0].id})
        }
      }
      else
      {
        this.setState({
              isCreated:true
          })
      }
    },
    publishNews:function(){
      this.setState({
              isPublishNews:PublishNewsStore.getPublishNewsResponse()
          })
      if(this.state.isPublishNews){
              alert("Succesfully Published news.....")
              if(!this.state.isRemoved){
                RemoveNewsFromQueueAction.removeAction({"id":this.props.params.id})
              }
      }
    },
    removeNewsHandler:function(){
      console.log("remove news")
      if(!this.state.isRemoved){
          if(confirm("Press Ok to Remove!") == true){
            RemoveNewsFromQueueAction.removeAction({"id":this.props.params.id})
          }
      }
    },
    removeNews:function(){
      this.setState({
              isRemoved:RemoveNewsFromQueueStore.getRemoveResponse()
          })
      if(this.state.isRemoved){
              window.location="/#/pendingNews";
      }
    },
    handleImage:function(test){
          var imageUrl = $("#imageUrl").val()
          document.getElementById("imageid").src=imageUrl;
    },
    render: function(){
      if(this.state.isInitial){
        return ( 
                  <div className="loader container-fluid"></div>
                );
      };
      var dataEditor = this.state.NewsEditorResult.data[0];
      console.log("dataEditor",dataEditor)
      var buttons = [ 
                        { "onClickCallback" : this.publishNewsHandler , "label" : "Publish", "className" : "control-btn"},
                        { "onClickCallback" : this.removeNewsHandler , "label" : "Remove" , "className" : "control-btn" }
                    ];
      var images = dataEditor.images
            var option = []
            var defaultSelected = <option selected="selected">{dataEditor.imageUrl}</option>
            option.push(defaultSelected)
            for(var i in images){
                if(dataEditor.imageUrl!=images[i]){
                    var item = <option>{images[i]}</option>
                }
                option.push(item)
            }
        return ( 
                <div>
                   
                      <ul className="vertical-list">
                        <li>
                          <label>Title:</label><input type="text" id="title"size="30" defaultValue={dataEditor.title}></input>
                        </li>
                        <li>
                          <label>Link:</label><input type="text" id="link" size="30" value={dataEditor.link}></input>
                        </li>
                        <li>
                          <label>Tags:</label><input type="text" id="tags" size="30" defaultValue={dataEditor.tags} placeholder="comma separated tags)"></input>
                        </li>
                        <li>
                          <label>ImageUrl:</label><select className="dropdown" id ="imageUrl" onChange={this.handleImage}>
                          <optgroup label="Select Images">
                            {option}
                          </optgroup>
                          <optgroup label="Select Default Images">
                            <option>https://www.ucl.ac.uk/big-data/bdi/images/data-head.jpg</option>
                            <option>http://cdn.fleishmanhillard.com/wp-content/uploads/2013/05/Big-Data1.jpg</option>
                            <option>http://cdn2.hubspot.net/hub/164625/file-499390915-jpg/images/big-data-for-business.jpg</option>
                            <option>http://www.pcquest.com/wp-content/uploads/2016/01/meraevents.com-6.jpg</option>
                            <option>http://live.worldbank.org/sites/default/files/big-data-promo-photo-sm2015v2.jpg</option>
                            <option>http://core0.staticworld.net/images/article/2015/06/big_data_cto-100593864-primary.idge.jpg</option>
                          </optgroup>
                            </select>
                        </li>
                        <li>
                          <label>Author:</label><input type="text" id="auther" size="30" defaultValue={dataEditor.author}></input>
                        </li>
                      </ul>
                      <div className="news-image">
                        <img src={dataEditor.imageUrl} alt="No image Available" className="" id ="imageid"/>
                      </div>
                    <div className="editor-block">
                      <NewsEditor
                        buttons={buttons}
                        data={dataEditor}
                      />
                    </div>
                      <div className="footer-action">
                      {this.state.isCreated && <div>Already Published</div>}
                      {this.state.isPublishNews && <div>Succesfully Published News</div>}
                      {this.state.isRemoved && <div>Succesfully Removed News</div>}
                    </div>
                </div>
                );
      }
});

module.exports = PendingNewsEditorWrapper;
