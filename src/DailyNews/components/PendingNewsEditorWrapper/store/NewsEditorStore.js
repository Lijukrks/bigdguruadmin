var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/NewsEditorConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';


var NewsEditorPage = {
    data:[]
}
function parseResponse(resp){
    NewsEditorPage = {
      data:[]
    }
    var jsonData = resp.hits.hits
    for(var i=0;i<jsonData.length;i++){
        var content={}
        if(jsonData[i]._source && jsonData[i]._source.content)
        {
            content.html = jsonData[i]._source.content
        }
        if(jsonData[i]._source && jsonData[i]._source.date)
        {
            content.date = jsonData[i]._source.date
        }
        if(jsonData[i]._source && jsonData[i]._source.title)
        {
            content.title = jsonData[i]._source.title
        }
        if(jsonData[i]._source && jsonData[i]._source.image)
        {
            content.imageUrl = jsonData[i]._source.image
        }
        if(jsonData[i]._source && jsonData[i]._source.link)
        {
            content.link = jsonData[i]._source.link
        }
        if(jsonData[i]._source && jsonData[i]._source.images)
        {
            content.images = jsonData[i]._source.images
        }
        if(jsonData[i]._source && jsonData[i]._source.tags)
        {
            content.tags = jsonData[i]._source.tags
        }
        if(jsonData[i]._source && jsonData[i]._source.author)
        {
            content.author = jsonData[i]._source.author
        }
        if(jsonData[i]._source && jsonData[i]._source.postedAuther)
        {
            content.postedAuther = jsonData[i]._source.postedAuther
        }
        if(jsonData[i]._id)
        {
            content.id = jsonData[i]._id
        }
        
         NewsEditorPage.data.push(content)
    }
    NewsEditorPage.totalHits=resp.hits.total
    
}

var NewsEditorStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getNewsEditorResponse:function(){
        return NewsEditorPage
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.NEWS_EDITOR_RESPONSE_RECIEVED:
            var resp = action.data;
            console.log("Store......",resp)
            parseResponse(resp)
            NewsEditorStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = NewsEditorStore;
