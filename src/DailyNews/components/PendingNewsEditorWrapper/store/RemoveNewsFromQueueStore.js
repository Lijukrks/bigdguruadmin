var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/NewsEditorConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';
var confirmationResponse = false
function parseResponse(resp){

    if(resp.found){
            confirmationResponse=resp.found
    }
}

var RemoveNewsFromQueueStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getRemoveResponse:function(){
        return confirmationResponse
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.REMOVE_NEWS_RESPONSE_RECIEVED:
            var resp = action.data;
            console.log("Storeremove......",resp)
            parseResponse(resp)
            RemoveNewsFromQueueStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = RemoveNewsFromQueueStore;
