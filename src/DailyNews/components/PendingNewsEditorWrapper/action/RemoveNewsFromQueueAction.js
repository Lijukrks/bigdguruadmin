import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PendingNewsEditorWrapper/constants/NewsEditorConstants';
import config from 'utils/config'

var RemoveNewsFromQueueAction = function(){

}


RemoveNewsFromQueueAction.prototype = {
	 removeAction:function(id){
		    $.ajax({
					url: config.server + 'RemoveNewsFromQueue',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(id),
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.REMOVE_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new RemoveNewsFromQueueAction();
