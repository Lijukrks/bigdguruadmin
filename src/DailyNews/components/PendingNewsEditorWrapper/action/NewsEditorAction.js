import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PendingNewsEditorWrapper/constants/NewsEditorConstants';
import config from 'utils/config'

var NewsEditorAction = function(){

}


NewsEditorAction.prototype = {
	 EditorAction:function(searchObject){
	 	var searchKeyword = searchObject?searchObject:{}
		    $.ajax({
					url: config.server + 'NewsEditor',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(searchKeyword),
					success: function(resp){
						console.log("NewsEditorAction......",resp)
							AppDispatcher.dispatch({
					        actionType: Constants.NEWS_EDITOR_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new NewsEditorAction();
