import React from 'react';
import 'public/css/signIn.css'
import SignUpAction from 'components/SignUp/action/SignUpAction'
import SignUpStore from 'components/SignUp/store/SignUpStore'
import { hashHistory } from 'react-router'
var crypto = require('crypto');
var SignUp = React.createClass({
	getInitialState:function(){
		return{
			isSignUp:false
		}
	},
	onSignUp:function(){
		var FirstName = $("#FirstName").val()
		var LastName = $("#LastName").val()
		var Mail = $("#Mail").val()
		var password = $("#password").val()
		var hash = crypto.createHmac('sha256', password)
                   .update('I love cupcakes')
                   .digest('hex');
					console.log(hash);
		SignUpAction.signUp({
			"FirstName":FirstName,
			"LastName":LastName,
			"Mail":Mail,
			"password":password
		})
	},
	componentWillMount:function(){
		SignUpStore.bind(this.onResponse)
	},
    onResponse:function(){
    	this.setState({
    		isSignUp:SignUpStore.getSignUpResponse()
    	})
    	if(this.state.isSignUp){
    		alert("Succesfull....")
    		hashHistory.push("/SignIn");
    	}
    },
    render: function(){
    		return (
    		<div className="outer-wrapper"> 
        			<div className="login-wrapper">
				        <div className="login">
				            <h2>Sign up</h2>
				            <ul>
				                <li>
				                    <input id="FirstName" type="text" placeholder="First Name" autofocus/>
				                </li>
				                <li>
				                    <input id="LastName" type="text" placeholder="Last Name" />
				                </li>
				                <li>
				                    <input id="Mail" type="text" placeholder="Mail Id" />
				                </li>
				                <li>
				                    <input id="password" type="password" placeholder="Password" />
				                </li>
				                <li>
				                    <input id="ConfirmPassword" type="password" placeholder="Conforme Password" />
				                </li>
				                <li>
				                    <span><button onClick={this.onSignUp}>Sign up</button></span>
				                </li>
				            </ul>
				        </div>
				    </div>
				     </div>
				    )
        
    }
});

module.exports = SignUp;
