import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PublishNews/constants/PublishNewsConstants';
import config from 'utils/config'

var PublishNewsAction = function(){

}


PublishNewsAction.prototype = {
	 searchAction:function(searchObject){
	 	var searchKeyword = searchObject?searchObject:{}
		    $.ajax({
					url: config.server + 'viewPublishedNews',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(searchKeyword),
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.PUBLISH_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new PublishNewsAction();
