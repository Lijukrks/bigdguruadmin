import React from 'react';
import PublishNewsAction from 'components/PublishNews/action/PublishNewsAction'
import PublishNewsStore from 'components/PublishNews/store/PublishNewsStore'
import NewsTable from 'components/NewsTable'
require('rc-pagination/assets/index.css');
var Pagination = require('rc-pagination');
var moment = require('moment');
var PublishNews = React.createClass({
	getInitialState:function(){
       return{
          SearchResult:PublishNewsStore.getSearchResponse(),
          isInitial:true,
          defaultCurrent:1
       }
    },
	componentWillMount:function(){
		PublishNewsAction.searchAction({'searchKeyword':"","pageNumber":0})
		PublishNewsStore.bind(this.SearchResponse)

	},
  componentWillUnMount:function(){
      PublishNewsStore.unbind(this.SearchResponse)
    },
	SearchResponse:function(){
		this.setState({
              SearchResult:PublishNewsStore.getSearchResponse(),
              isInitial:false
          })
	},

  onPageChange: function(pageNum){
    this.setState({
              isInitial:true,
              defaultCurrent:pageNum
          })
    pageNum = (pageNum - 1) * 20;
    PublishNewsAction.searchAction({'searchKeyword':"","pageNumber":pageNum})
  },

    render: function(){
          var Queue = this.state.SearchResult.data;
          console.log("Queue........liju",Queue)
          var NewsEditor = "PublishNewsEditorWrapper"
          var columns = [ "Title" , "Date","Posted Author","Published Author","Author","Action"]
          var data = []
          var singleRow ={}
          for(var i = 0, n = Queue.length; i < n ; i++){
            singleRow={}
            singleRow ={"Date":moment(Queue[i].date).format("YYYY-MM-DD"),
                        "Title":Queue[i].title,
                        "id":Queue[i].id,
                        "Author":Queue[i].author,
                        "PostedAuthor":Queue[i].postedAuther,
                        "PublishedAuthor":Queue[i].pablishedAuther
          }
                     data.push(singleRow)
                   }
        if(this.state.isInitial) {
          return(<div className="loader container-fluid"></div>)
        } 
        return ( <div>
                  <label>Total Published News {this.state.SearchResult.totalHits}</label>
                  <NewsTable
                    data={data}
                    columns={columns}
                    editor={NewsEditor} 
                  />
                  <Pagination 
                      defaultCurrent={this.state.defaultCurrent} 
                      total={this.state.SearchResult.totalHits/2}
                      onChange={this.onPageChange}
                  />
                    </div>
                );
    }
});

module.exports = PublishNews;
