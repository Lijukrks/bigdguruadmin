var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/PendingNewsConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';


var pendingNewsPage = {
    data:[]
}
function parseResponse(resp){
    console.log("resp",resp)
    pendingNewsPage = {
      data:[]
    }
    if(resp.hits.hits.length){
        var jsonData = resp.hits.hits
    for(var i=0;i<jsonData.length;i++){
        var content={}
        if(jsonData[i]._source && jsonData[i]._source.title)
        {
            content.title = jsonData[i]._source.title
        }
        if(jsonData[i]._source && jsonData[i]._source.publishedDate)
        {
            content.date = jsonData[i]._source.publishedDate
            console.log("content.date",content.date)
        }
        if(jsonData[i]._source && jsonData[i]._source.html)
        {
            content.html = jsonData[i]._source.html
        }
        if(jsonData[i]._source && jsonData[i]._source.imageUrl)
        {
            content.imageUrl = jsonData[i]._source.imageUrl
        }
        if(jsonData[i]._source && jsonData[i]._source.author)
        {
            content.author = jsonData[i]._source.author
        }
        if(jsonData[i]._source && jsonData[i]._source.postedAuther)
        {
            content.postedAuther = jsonData[i]._source.postedAuther
        }
        if(jsonData[i]._id)
        {
            content.id = jsonData[i]._id
        }
        
         pendingNewsPage.data.push(content)
    }
    pendingNewsPage.totalHits=resp.hits.total
    
    }
    
}

var PendingNewsStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getQueueResponse:function(){
        return pendingNewsPage
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.PENDING_NEWS_RESPONSE_RECIEVED:
            var resp = action.data;
                                    console.log("Store......",resp)

            parseResponse(resp)
            PendingNewsStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = PendingNewsStore;
