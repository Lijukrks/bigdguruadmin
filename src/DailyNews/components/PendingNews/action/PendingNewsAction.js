import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PendingNews/constants/PendingNewsConstants';
import config from 'utils/config'

var PendingNewsAction = function(){

}


PendingNewsAction.prototype = {
	 QueueAction:function(searchObject){
	 	var searchKeyword = searchObject?searchObject:{}
		    $.ajax({
					url: config.server + 'viewPendingNews',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(searchKeyword),
					success: function(resp){
						console.log("action......",resp)
							AppDispatcher.dispatch({
					        actionType: Constants.PENDING_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new PendingNewsAction();
