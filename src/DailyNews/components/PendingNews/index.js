import React from 'react';
import PendingNewsAction from 'components/PendingNews/action/PendingNewsAction'
import PendingNewsStore from 'components/PendingNews/store/PendingNewsStore'
import NewsTable from 'components/NewsTable'
require('rc-pagination/assets/index.css');
var Pagination = require('rc-pagination');

var PendingNews = React.createClass({
	getInitialState:function(){
       return{
          QueueResult:PendingNewsStore.getQueueResponse(),
          isInitial:true,
          defaultCurrent:1
       }
    },
	componentWillMount:function(){
		PendingNewsAction.QueueAction({'searchKeyword':"","pageNumber":0})
		PendingNewsStore.bind(this.QueueResponse)

	},
  componentWillUnMount:function(){
      PendingNewsStore.unbind(this.QueueResponse);
  },
	QueueResponse:function(){
		this.setState({
              QueueResult:PendingNewsStore.getQueueResponse(),
              isInitial:false
          });
	},
  onPageChange: function(pageNum){
    this.setState({
              isInitial:true,
              defaultCurrent:pageNum
          });
    pageNum = (pageNum - 1) * 20;
    console.log(pageNum)
    PendingNewsAction.QueueAction({'searchKeyword':"","pageNumber":pageNum})
  },
    render: function(){
          console.log("Queue",this.state.QueueResult.data)
          var Queue = this.state.QueueResult.data;
          var columns = [ "Title" , "Date","Posted Author","Real Author","Action"]
          var NewsEditor = "PendingNewsEditorWrapper"
          var data = []
          var singleRow ={}
          for(var i = 0, n = Queue.length; i < n ; i++){
            singleRow={}
            singleRow ={
                        "Date":Queue[i].date,
                        "Title":Queue[i].title,
                        "id":Queue[i].id,
                        "Author":Queue[i].author,
                        "PostedAuthor":Queue[i].postedAuther
                        }
                     data.push(singleRow)
                   }
        if(this.state.isInitial) {
          return(<div className="loader container-fluid"></div>)
        }
        return (  
                  <div>
                  <label>Total Pending News {this.state.QueueResult.totalHits}</label>
                    <NewsTable
                      data={data}
                      columns={columns} 
                      editor={NewsEditor}
                    />
                    <Pagination 
                      defaultCurrent={this.state.defaultCurrent} 
                      total={this.state.QueueResult.totalHits/2}
                      onChange={this.onPageChange}
                    />
                  </div> 
                );
    }
});

module.exports = PendingNews;

