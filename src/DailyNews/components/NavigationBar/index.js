import React from 'react';
import { render } from 'react-dom'
import {Link} from 'react-router'
import TabStateStore from 'components/MainFrame/store/TabStateStore'
var NavigationBar = React.createClass({
    getInitialState:function(){
        return{
            addNews:"selected",
            pendingNews:"",
            publishNews:"",
            Questions:"",
            forums:""
        }
    },
    componentWillMount:function(){
        TabStateStore.bind(this.onTabState)
        console.log("TabStateStore.getMinorTab()",TabStateStore.getMinorTab())
        if(TabStateStore.getMinorTab()=="/addNews"){
            this.setState({
                addNews:"selected",
                pendingNews:"",
                publishNews:"",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/pendingNews"){
            this.setState({
                addNews:"",
                pendingNews:"selected",
                publishNews:"",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/publishNews"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"selected",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/Questions"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"",
                Questions:"selected",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/forums"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"",
                Questions:"",
                forums:"selected"
            })
        }
    },
    onTabState:function(){
        if(TabStateStore.getMinorTab()=="/addNews"){
            this.setState({
                addNews:"selected",
                pendingNews:"",
                publishNews:"",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/pendingNews"){
            this.setState({
                addNews:"",
                pendingNews:"selected",
                publishNews:"",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/publishNews"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"selected",
                Questions:"",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/Questions"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"",
                Questions:"selected",
                forums:""
            })
        }
        if(TabStateStore.getMinorTab()=="/forums"){
            this.setState({
                addNews:"",
                pendingNews:"",
                publishNews:"",
                Questions:"",
                forums:"selected"
            })
        }
    },
    render: function(){
        return ( 
                <div className="navigation">
                    <ul className="left-menu">
                        <li className={this.state.addNews}>
                            <Link to="/addNews">
                                Add News
                            </Link>
                        </li>
                        <li className={this.state.pendingNews}>
                        	<Link to="/pendingNews">
                        		Pending News
                        	</Link>
                        </li>
                        <li className={this.state.publishNews}>
                        	<Link to="/publishNews">
                        		News
                        	</Link>
                        </li>
                        <li className={this.state.forums}>
                            <Link to="/forums">
                                Forums
                            </Link>
                        </li>
                    </ul>
                </div>
                    );
    }
});

module.exports = NavigationBar;
