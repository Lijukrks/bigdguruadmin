import React from 'react';
import TinyMCE from 'react-tinymce';
var cookie = require("public/js/cookie.js")
var moment = require('moment')
var newscontent={}
var NewsEditor = React.createClass({

  handleEditorChange:function(e) {
    newscontent.content = e.target.getContent()
    var text = e.target.getContent()
    text = text.replace(/<[^>]*>/g,'');
    newscontent.htmlstrip = text
    newscontent.title = $("#title").val()
    newscontent.image = $("#imageUrl").val()
    newscontent.link = $("#link").val()
    var tags = $("#tags").val()
    tags = tags + '';
    tags = tags.split(',');
    newscontent.tags = tags
    newscontent.author = $("#auther").val()
    newscontent.postedAuther = cookie.readCookie("Username")
    newscontent.publishedDate = this.props.data.date
    newscontent.postedDate = moment().format("DD/MM/YYYY");
  },
  componentDidMount:function(){
    if(this.props.data.html){
      var text = this.props.data.html
      text = text.replace(/<[^>]*>/g,'');
      newscontent.htmlstrip = text
      newscontent.content = this.props.data.html
    }
    else{
      var text = this.props.data.text
      text = text.replace(/<[^>]*>/g,'');
      newscontent.htmlstrip = text
      newscontent.content = this.props.data.text
    }
    newscontent.title = $("#title").val()
    newscontent.image = $("#imageUrl").val()
    newscontent.link = $("#link").val()
    var tags = $("#tags").val()
    tags = tags + '';
    tags = tags.split(',');
    newscontent.tags = tags
    newscontent.author = $("#auther").val()
    newscontent.postedAuther = cookie.readCookie("Username")
    newscontent.publishedDate = this.props.data.date
    newscontent.postedDate = moment().format("DD/MM/YYYY");
  },
  render: function(){
    console.log("this.props.data.date",this.props.data.date)
      $(document).keyup(function(){
        newscontent.title = $("#title").val()
        newscontent.image = $("#imageUrl").val()
        newscontent.link = $("#link").val()
        var tags = $("#tags").val()
        tags = tags + '';
        tags = tags.split(',');
        newscontent.tags = tags
        newscontent.author = $("#auther").val()
        newscontent.postedAuther = cookie.readCookie("Username")
        newscontent.publishedDate = this.props.data.date
        newscontent.postedDate = moment().format("DD/MM/YYYY");

      })
      var buttons = [];
      for(var idx in this.props.buttons){
        var button = this.props.buttons[idx];
        buttons.push(
          <button 
              key={idx}
              type="button" 
               onClick={button.onClickCallback.bind(this,newscontent)} 
              className={button.className}>
              {button.label}
          </button>
        )
      }
      if(this.props.data.html){
        var contentData=this.props.data.html;
      }
      else{
        var contentData = this.props.data.text
      }

        return ( 
                    <div>
                      {buttons}
                      <TinyMCE
                        content={contentData}
                        config={{
                          plugins: 'autolink link image lists print preview',
                          toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                        }}
                        onChange={this.handleEditorChange}
                      />
                    </div>
              );
      }
});

module.exports = NewsEditor;
