import React from 'react';
import {Link} from 'react-router'

var NewsTable = React.createClass({
  

    render: function(){
    	var singleRow = []
    	var TableHead = []
    	var TableContent=this.props.data
    	var columns = this.props.columns
    	for(var i=0;i<columns.length;i++){
    		var item=<th key ={i}>
	                  			{columns[i]}
	                 </th>
	        TableHead.push(item)
    	}
      if(columns.length==5){
        for(var i=0;i<TableContent.length;i++){
        var item=<tr key ={i}>
                    <td>
                      {TableContent[i].Title}
                    </td>
                    <td>
                      {TableContent[i].Date}
                    </td>
                    <td>
                      {TableContent[i].PostedAuthor}
                    </td>
                    <td>
                      {TableContent[i].Author}
                    </td>
                    <td>
                     <Link to={"/"+this.props.editor+"/"+TableContent[i].id}>Edit</Link></td>
                  </tr>
        singleRow.push(item)
        }
      }
      else{
      	for(var i=0;i<TableContent.length;i++){
      		var item=<tr key ={i}>
                    	<td>
                    		{TableContent[i].Title}
                    	</td>
                    	<td>
                    		{TableContent[i].Date}
                    	</td>
                      <td>
                        {TableContent[i].PostedAuthor}
                      </td>
                      <td>
                        {TableContent[i].PublishedAuthor}
                      </td>
                      <td>
                        {TableContent[i].Author}
                      </td>
                    	<td>
                    	 <Link to={"/"+this.props.editor+"/"+TableContent[i].id}>Edit</Link></td>
                    </tr>
      		singleRow.push(item)

      	}
      }
        return ( 
        		<table>
        				<thead>
	                  	<tr>
	                  		{TableHead}
	                  	</tr>
	                  </thead>
	                  <tbody>
	                  		{singleRow}
	                  </tbody>
        		</table>
                );
    }
});

module.exports = NewsTable;
