var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/SignInConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';
var confirmationSignUp
function parseResponse(resp){
    confirmationSignUp = resp
}

var SignInStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getSignInResponse:function(){
        return confirmationSignUp
    }
});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.SIGN_IN_RESPONSE_RECIEVED:
            var resp = action.data;
            parseResponse(resp)
            console.log("SignInStore......",resp)
            SignInStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = SignInStore;
