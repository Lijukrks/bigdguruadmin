import React from 'react';
import 'public/css/signIn.css'
import SignInAction from 'components/SignIn/action/SignInAction'
import SignInStore from 'components/SignIn/store/SignInStore'
import {Link} from 'react-router'
import { hashHistory } from 'react-router'
var cookie = require("public/js/cookie.js")
var SignUp = React.createClass({
	getInitialState:function(){
		return{
			isSignIn:'',
            cookie:cookie.readCookie("liju")
		}

	},
    componentWillMount:function(){
        SignInStore.bind(this.onResponse)
        this.setState({
            cookie:cookie.readCookie("liju")
        })
    },
    componentWillUnMount:function(){
    	SignInStore.unbind(this.onResponse)
    },
    onResponse:function(){
    	this.setState({
    		isSignIn:SignInStore.getSignInResponse()
    	})
        if(this.state.isSignIn){
            console.log("sdhhgfhgsfdhgfsdgh...............",this.state.isSignIn)
            cookie.createCookie("liju",this.state.isSignIn,2)
            cookie.createCookie("Username",this.state.isSignIn.name,2)
            hashHistory.push("/mainFrame");
        }
    },
    onSignIn:function(){
        var Mail = $("#Mail").val()
        var password = $("#password").val()
        console.log("mail",Mail)
        console.log("password",password)
        SignInAction.signIn({
            "Mail":Mail,
            "password":password
        })
    },
    render: function(){
       
        return ( 
        			<div className="outer-wrapper">
                    <div className="login-wrapper">
				        <div className="login">
				            <h2>Login</h2>
				            <ul>
				                <li>
				                    <input id="Mail" type="text" placeholder="Username" autofocus/>
				                </li>
				                <li>
				                    <input id="password" type="password" placeholder="Password" />
				                </li>
				                <li>
				                    <span><a href="#">Forgot your password?</a></span>
				                    <span>
                                    <button onClick={this.onSignIn}>Log in</button>
                                    </span>
				                </li>
				                <li className="text-center">
				                    <Link to="/SignUp">Create account </Link>
				                </li>
				            </ul>
				        </div>
				    </div>
                    </div>

            );
    }
});

module.exports = SignUp;
