import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SignIn/constants/SignInConstants';
import config from 'utils/config'

var SignInAction = function(){
	
}
SignInAction.prototype = {
	 signIn:function(urlObject){
	 	var urlObject = urlObject?urlObject:{}
		    $.ajax({
					url: config.server + 'SignIn',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(urlObject),
					success: function(resp){
						console.log("SignInAction.............",resp)
							AppDispatcher.dispatch({
					        actionType: Constants.SIGN_IN_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new SignInAction();
