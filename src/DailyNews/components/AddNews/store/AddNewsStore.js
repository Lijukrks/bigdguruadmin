var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/AddNewsConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';


var confirmationResponse = null
function parseResponse(resp){
    console.log()
   if(resp.created){
        confirmationResponse= resp.created
    }
}

var AddNewsStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getAddNewsResponse:function(){
        return confirmationResponse
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.ADD_NEWS_RESPONSE_RECIEVED:
            var resp = action.data;
            console.log("Store......",resp)
            parseResponse(resp)
            AddNewsStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = AddNewsStore;
