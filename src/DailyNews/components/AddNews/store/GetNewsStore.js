var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/AddNewsConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';
var moment = require('moment')

var GetNewsPage = null
function parseResponse(resp){
    GetNewsPage = JSON.parse(resp)
    GetNewsPage.date = moment(GetNewsPage.date).format("DD/MM/YYYY")
    console.log("responsestore....",)
}

var GetNewsStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getResponse:function(){
        return GetNewsPage
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.GET_NEWS_RESPONSE_RECIEVED:
            var resp = action.data;
            console.log("GetNewsStore......",resp)
            parseResponse(resp)
            GetNewsStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = GetNewsStore;
