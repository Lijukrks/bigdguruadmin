import React from 'react';
import NewsEditor from 'components/NewsEditor'
import AddNewsAction from 'components/AddNews/action/AddNewsAction'
import AddNewsStore from 'components/AddNews/store/AddNewsStore'
import GetNewsAction from 'components/AddNews/action/GetNewsAction'
import GetNewsStore from 'components/AddNews/store/GetNewsStore'
import { hashHistory,Router } from 'react-router'

var cookie = require("public/js/cookie.js")
var AddNews = React.createClass({
     mixins: [Router.Navigation],
    getInitialState:function(){
       return{
          getResult:'',
          isInitial:false,
          isAdded:false,
          isFailed:false,
       }
    },
	OnSubmit:function(){
		var url = $("#articleUrl").val()
        if(url){
            GetNewsAction.getAction({"url":url,"token":cookie.readCookie("liju")})
            this.setState({
                isInitial:true
            })
        }
	},
    componentWillMount:function(){
        GetNewsStore.bind(this.onResponse)
        AddNewsStore.bind(this.onAddNews)
    },
    componentWillUnMount:function(){
        GetNewsStore.unbind(this.onResponse)
        AddNewsStore.unbind(this.onAddNews)
    },
    onResponse:function(){
        if(GetNewsStore.getResponse()!="Bad request"){
            this.setState({
                getResult:GetNewsStore.getResponse(),
                isInitial:false,
                isFailed:false
            })
        }  
        else{
            this.setState({
                getResult:false,
                isInitial:false,
                isFailed:true

            })
        } 
    },
    onAddNews:function(){
        this.setState({
            isAdded:AddNewsStore.getAddNewsResponse()
        })
        if(this.state.isAdded){
            window.location="/#/pendingNews";
        }
    },
    addNewsHandler:function(newsBody){
        newsBody.images = this.state.getResult.images
        console.log("newsBody",newsBody)
        if(confirm("Press Ok to Publish!") == true){
            newsBody.link = $("#articleUrl").val()
            AddNewsAction.addAction({"newsBody":newsBody});
        }
    },
    handleKeyPress:function(e){
        if (e.key === 'Enter') {
            var url = $("#articleUrl").val()
            if(url){
                GetNewsAction.getAction({"url":url,"token":cookie.readCookie("liju")})
                this.setState({
                    isInitial:true
                })
            }
        }
    },
    componentDidMount:function(){
      React.findDOMNode(this.refs.urlInput).focus(); 
    },
    handleImage:function(test){
        var imageUrl = $("#imageUrl").val()
        document.getElementById("imageid").src=imageUrl;
    },
    onCancel:function(){
        window.location="/#/pendingNews";
    },
    render: function(){
        if(this.state.getResult){
            var buttons = [ 
                        { "onClickCallback" : this.addNewsHandler , "label" : "Publish", "className" : "control-btn"}
                    ];
            var dataEditor = this.state.getResult
            console.log("dataEditor",dataEditor.images)
            var images = dataEditor.images
            var option = []
            var defaultSelected = <option selected="selected">{dataEditor.imageLink}</option>
            option.push(defaultSelected)
            for(var i in images){
                if(dataEditor.imageLink!=images[i]){
                    var item = <option>{images[i]}</option>
                }
                option.push(item)
            }
            var newsEditor = <NewsEditor
                        buttons={buttons}
                        data={dataEditor}
                             />
            var item =<span>
                        <span className="cancel-btn">
                        <button className = "btn btn-default search-block button" onClick={this.onCancel}>Cancel</button>
                        </span>
                      <ul className="vertical-list">
                        <li>
                          <label>Title:</label><input type="text" id="title"size="30" defaultValue={dataEditor.title}></input>
                        </li>
                        <li>
                          <label>Tags:</label><input type="text" id="tags" size="30" defaultValue={dataEditor.tags} placeholder="comma separated tags)"></input>
                        </li>
                        <li>
                          <label>Author:</label><input type="text" id="auther" size="30" defaultValue={dataEditor.author}></input>
                        </li>
                        <li>
                          <label>ImageUrl:</label><select className="dropdown" id ="imageUrl" onChange={this.handleImage}>
                          <optgroup label="Select Images">
                          {option}
                          </optgroup>
                          <optgroup label="Select Default Images">
                            <option>https://www.ucl.ac.uk/big-data/bdi/images/data-head.jpg</option>
                            <option>http://cdn.fleishmanhillard.com/wp-content/uploads/2013/05/Big-Data1.jpg</option>
                            <option>http://cdn2.hubspot.net/hub/164625/file-499390915-jpg/images/big-data-for-business.jpg</option>
                            <option>http://www.pcquest.com/wp-content/uploads/2016/01/meraevents.com-6.jpg</option>
                            <option>http://live.worldbank.org/sites/default/files/big-data-promo-photo-sm2015v2.jpg</option>
                            <option>http://core0.staticworld.net/images/article/2015/06/big_data_cto-100593864-primary.idge.jpg</option>
                          </optgroup>
                            </select>
                        </li>
                      </ul>
                       <div className="news-image">
                        <img src={dataEditor.imageLink} alt="No image Available" className="" id ="imageid"/>
                      </div>
                      </span>
                    
        }
        if(this.state.isInitial){
            var item = <div className="loader container-fluid"></div>
            var newsEditor=[]
        }
        if(this.state.isFailed){
            var item = <h1>Failed to get the url information......</h1>
            var newsEditor=[]
        }
        return  (     
                    <div>
                    	<div className="search-block">
                            <label>Article Url</label>
                            <input id="articleUrl"  type="text" onKeyPress={this.handleKeyPress} ref="urlInput"></input>
                            <button onClick={this.OnSubmit}>
                                Submit
                            </button>
                        </div>
                        {item}
                        <div className="editor-block">
                            {newsEditor}
                        </div>
                        <div className="footer-action">
                            {this.state.isAdded && <div>Added to Queue</div>}
                        </div>
                    </div>

                );
    }  
});

module.exports = AddNews;








// <li>
//                           <label>ImageUrl:</label><input type="text" id="imageUrl" size="30" defaultValue={dataEditor.imageLink}></input>
//                         </li>