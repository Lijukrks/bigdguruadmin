import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AddNews/constants/AddNewsConstants';
import config from 'utils/config'

var GetNewsAction = function(){

}


GetNewsAction.prototype = {
	 getAction:function(urlObject){
	 	var urlObject = urlObject?urlObject:{}
		    $.ajax({
					url: config.server + 'GetNews',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(urlObject),
					headers:{token:urlObject.token},
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.GET_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new GetNewsAction();
