import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AddNews/constants/AddNewsConstants';
import config from 'utils/config'

var AddNewsAction = function(){

}


AddNewsAction.prototype = {
	 addAction:function(urlObject){
	 	alert()
	 	var urlObject = urlObject?urlObject:{}
		    $.ajax({
					url: config.server + 'AddNews',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(urlObject),
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.ADD_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new AddNewsAction();
