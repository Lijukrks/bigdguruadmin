var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
import Constants from 'components/MainFrame/constants/TabStateConstant';
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';

var majorTab;
var minorTab;

function parseResponse(resp){
   if(!resp || resp.length != 2){
   return;
   }
   majorTab = resp[0].path;
   minorTab = resp[1].path;
   
}

var TabStateStore = assign({},EventEmitter.prototype,{
   emitChangeEvent: function(event) {
       this.emit(event);
   },
   bind: function(callback) {
       this.on(RESPONSE_CHANGE_EVENT, callback);
   },
   unbind: function(callback) {
       this.removeListener(RESPONSE_CHANGE_EVENT, callback);
   },
   getMajorTab:function(){
        return majorTab;
   },
   getMinorTab:function(){
        return minorTab;
   }

});

Dispatcher.register(function(action){

   switch (action.actionType) {
       case Constants.TABSTATE_RESPONSE_RECIEVED:
           var resp = action.data;
           parseResponse(resp)
           console.log("resp store....",resp)
           TabStateStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
       default:
   }


});

module.exports = TabStateStore;