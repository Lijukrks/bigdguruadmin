import React from 'react';
import { hashHistory } from 'react-router'
import 'public/css/style.css'
import 'public/css/bootstrap.css'
import 'public/css/custom.css'
import 'public/css/accordion.css'
import NavigationBar from 'components/NavigationBar'
var cookie = require("public/js/cookie.js")

var MainFrame = React.createClass({
		onLogout:function(){
			cookie.eraseCookie("liju");
			cookie.eraseCookie("Username");
			hashHistory.push("/SignIn");
			console.log("logout cookie",cookie.readCookie("Username"))
		},
		render: function(){
				return ( 	
							<div>
							<header className="main-head clearfix">
							
							<div className="logo">
      						<a to="/"><img src="images/logo.png" alt="" /></a>
							</div>
							
							<label className="user-name">Hai {cookie.readCookie("Username")}</label>
								<button className="logout-btn" onClick={this.onLogout}>Logout</button>
								</header>
								<div className="wrapper">
								<NavigationBar />
								<div className="main-wrapper">
								<section className="content-wrapper">
									<div className="main-content">									
										{this.props.children}
									</div>
									</section>
								</div>
							</div>
							</div>
                );
		}
});

module.exports = MainFrame;
