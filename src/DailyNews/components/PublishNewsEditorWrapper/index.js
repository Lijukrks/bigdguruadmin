import React from 'react';
import NewsEditor from 'components/NewsEditor'
import PublishNewsEditorAction from 'components/PublishNewsEditorWrapper/action/PublishNewsEditorAction'
import PublishNewsEditorStore from 'components/PublishNewsEditorWrapper/store/PublishNewsEditorStore'
import UpdateNewsAction from 'components/PublishNewsEditorWrapper/action/UpdateNewsAction'
import UpdateNewsStore from 'components/PublishNewsEditorWrapper/store/UpdateNewsStore'
import RemoveNewsFromQueueAction from 'components/PublishNewsEditorWrapper/action/RemoveNewsFromQueueAction'
import RemoveNewsFromQueueStore from 'components/PublishNewsEditorWrapper/store/RemoveNewsFromQueueStore'
import { hashHistory } from 'react-router'
var PublishNewsEditorWrapper= React.createClass({

  getInitialState:function(){
       return{
          isInitial : true,
          NewsEditorResult:[],
          isRemoved:false,
          isUdateNews:false,
          isCreated:false,
       }
  },
  componentWillMount:function(){
      PublishNewsEditorStore.bind(this.onNewsResponse)
      RemoveNewsFromQueueStore.bind(this.onNewsRemoved)
      UpdateNewsStore.bind(this.updateNews)
      
      PublishNewsEditorAction.EditorAction({'searchKeyword':this.props.params.id})

  },
  componentWillUnMount:function(){
      PublishNewsEditorStore.unbind(this.onNewsResponse)
  },
  onNewsResponse : function(){
    this.setState({
              isInitial : false,
              NewsEditorResult:PublishNewsEditorStore.getPublishNewsEditorResponse()
          })
  },
  onNewsRemoved:function(){
    this.setState({
            isRemoved:RemoveNewsFromQueueStore.getRemoveResponse()
        })
    if(this.state.isRemoved){
            window.location="/#/publishNews"
    }
  },
  removeNewsHandler:function(){
    if(!this.state.isRemoved){
      if(confirm("Press Ok to Remove!") == true){
        RemoveNewsFromQueueAction.removeAction({"id":this.props.params.id})
      }
    }
  },
  updateNewsHandler:function(newsBody){
    if(!this.state.isUdateNews){
      if(confirm("Press Ok to Update!") == true){
        UpdateNewsAction.addAction({'newsBody':newsBody,"id":this.props.params.id})
      }
      
    }
    else
    {
      this.setState({
            isCreated:true
        })
    }
  },
  updateNews:function(){
    this.setState({
            isUdateNews:UpdateNewsStore.getUpdateNewsResponse()
        })
    if(this.state.isUdateNews){
            window.location="/#/publishNews"
    }
  },
    render: function(){
      if(this.state.isInitial){
        return ( 
                  <div className="loader container-fluid"></div>
                );
      };
      var dataEditor = this.state.NewsEditorResult.data[0];
      var buttons = [ 
                        { "onClickCallback" : this.updateNewsHandler , "label" : "Update", "className" : "control-btn"},
                        { "onClickCallback" : this.removeNewsHandler , "label" : "Remove" , "className" : "control-btn" }
                    ];
        return ( 
                <div>
                      <ul className="vertical-list">
                        <li>
                          <label>Title:</label><input type="text" id="title"size="30" defaultValue={dataEditor.title}></input>
                        </li>
                        <li>
                          <label>link:</label><input type="text" id="link" size="30" value={dataEditor.link}></input>
                        </li>
                        <li>
                          <label>tags:</label><input type="text" id="tags" size="30" defaultValue={dataEditor.tags} placeholder="comma separated tags)"></input>
                        </li>
                        <li>
                          <label>ImageUrl:</label><input type="text" id="imageUrl" size="30" defaultValue={dataEditor.imageUrl}></input>
                        </li>
                        <li>
                          <label>Author:</label><input type="text" id="auther" size="30" defaultValue={dataEditor.author}></input>
                        </li>
                      </ul>
                      <div className="news-image">
                        <img src={dataEditor.imageUrl} alt="No image Available" className=""/>
                      </div>
                    <div className="editor-block">
                      <NewsEditor
                        buttons={buttons}
                        data={dataEditor}
                      />
                    </div>
                      <div className="footer-action">
                      {this.state.isCreated && <div>Already Published</div>}
                      {this.state.isUdateNews && <div>Succesfully Upadated News</div>}
                      {this.state.isRemoved && <div>Succesfully Removed News</div>}
                    </div>
                </div>
                );
      }
});

module.exports = PublishNewsEditorWrapper;
