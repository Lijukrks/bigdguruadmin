import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PendingNewsEditorWrapper/constants/NewsEditorConstants';
import config from 'utils/config'

var PublishNewsEditorAction = function(){

}


PublishNewsEditorAction.prototype = {
	 EditorAction:function(searchObject){
	 	var searchKeyword = searchObject?searchObject:{}
		    $.ajax({
					url: config.server + 'PublishNewsEditor',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(searchKeyword),
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.NEWS_EDITOR_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new PublishNewsEditorAction();
