import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PendingNewsEditorWrapper/constants/NewsEditorConstants';
import config from 'utils/config'

var UpdateNewsAction = function(){

}


UpdateNewsAction.prototype = {
	 addAction:function(newsBody){
		    $.ajax({
					url: config.server + 'UpdateNews',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(newsBody),
					success: function(resp){
							AppDispatcher.dispatch({
					        actionType: Constants.ADD_NEWS_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new UpdateNewsAction();
