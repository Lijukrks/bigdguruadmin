var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/NewsEditorConstants');
var assign = require('object-assign');
var RESPONSE_CHANGE_EVENT = 'globalResponse';
var confirmationResponse = false
function parseResponse(resp){

    if(resp.created){
            confirmationResponse= resp.created
    }
}

var PublishNewsStore = assign({},EventEmitter.prototype,{
    emitChangeEvent: function(event) {
        this.emit(event);
    },
    bind: function(callback) {
        this.on(RESPONSE_CHANGE_EVENT, callback);
    },
    unbind: function(callback) {
        this.removeListener(RESPONSE_CHANGE_EVENT,callback);
    },
    getUpdateNewsResponse:function(){
        return confirmationResponse
    }
    

});

Dispatcher.register(function(action){
    switch (action.actionType) {
        case Constants.ADD_NEWS_RESPONSE_RECIEVED:
            var resp = action.data;
            parseResponse(resp)
            PublishNewsStore.emitChangeEvent(RESPONSE_CHANGE_EVENT)
        default:
    }


});

module.exports = PublishNewsStore;
