import React from 'react';
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var MainFrame = require("./components/MainFrame");
var AddNews = require("./components/AddNews")
var PendingNews = require("./components/PendingNews")
var PublishNews = require("./components/PublishNews")
var PendingNewsEditorWrapper = require("./components/PendingNewsEditorWrapper")
var PublishNewsEditorWrapper = require("./components/PublishNewsEditorWrapper")
var Forums = require('./components/Forums')
var SignUp = require("./components/SignUp")
var SignIn = require("./components/SignIn")
import TabStateAction from './components/MainFrame/action/TabStateAction'
import { hashHistory,Redirect,IndexRoute} from 'react-router'
var cookie = require("public/js/cookie.js")
function requireAuth(nextState, replace){
	if (!cookie.readCookie("liju")) {
    replace({
      pathname: '/SignIn',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}
function routerChange(stateChangeTo){
	TabStateAction.isClicked(stateChangeTo.routes)
}
ReactDOM.render(
				<Router history={hashHistory} >
			        <Redirect from="/" to="/mainFrame" />
			        <Route path="/mainFrame" component={MainFrame} onEnter={requireAuth}>
			        	<IndexRoute component={AddNews} />
						<Route path="/addNews" component={AddNews} onEnter={routerChange}/>
						<Route path="/pendingNews" component={PendingNews} onEnter={routerChange}/>
						<Route path="/publishNews" component={PublishNews} onEnter={routerChange}/>
						<Route path="/PendingNewsEditorWrapper/:id" component={PendingNewsEditorWrapper} />
						<Route path="/PublishNewsEditorWrapper/:id" component={PublishNewsEditorWrapper} />
						<Route path="/addNews" component={AddNews} onEnter={routerChange}/>
						<Route path="/forums" component={Forums} onEnter={routerChange}/>
					</Route>
			        <Route path="/SignIn" component={SignIn}/>
			        <Route path="/SignUp" component={SignUp}/>
		        </Router>
					,document.getElementById('renderData')
				);